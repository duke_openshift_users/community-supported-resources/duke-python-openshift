FROM duke-debian:latest

LABEL maintainer='Nate Childers <nate.childers@duke.edu>' \
      vendor='Duke University, OIT SSI-Systems' \
      architecture='x86_64' \
      summary='base image with python + openshift library' \
      description='Base image for using openshift python library' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/oit-ssi-systems/openshift/ops-images' 

LABEL version='1.0' \
      release='1'

RUN apt-get update && \
    apt-get upgrade -y

RUN apt-get install -y ca-certificates \
                    python3 \
                    python3-pip && \
    apt-get autoclean && \
    apt-get autoremove -y

RUN pip3 install openshift
